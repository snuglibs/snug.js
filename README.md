# Snug.js

Snug for vanilla javascript.

## CDN Url(s)

-   https://cdn.jsdelivr.net/gh/snuglibs/snug.js/snug.min.js (Testing)
    -   https://cdn.jsdelivr.net/gh/snuglibs/snug.js@0.0.1/snug.min.js (Stable Release)

## Example(s)

### Snug -> JSON

```js
let source = new Snug('users [(? name "aiocat" id 0) (? name "john" id 1)]');
console.log(source.parseJson()); // {"users":[{"name":"aiocat","id":0},{"name":"john","id":1}]}
```

### Snug -> Object

```js
let source = new Snug('users [(? name "aiocat" id 0) (? name "john" id 1)]');
let obj = JSON.parse(source.parseJson());
console.log(obj); // Object { users: (2) […] }
```

### Object -> Snug

```js
let source = {
    users: [
        {
            name: "aiocat",
            id: 0,
        },
        {
            name: "john",
            id: 1,
        },
    ],
};

console.log(toSnug(source)); // users [(? name "aiocat"  id 0  ) (? name "john"  id 1  )]
```

### JSON -> Snug

```js
let source = JSON.parse(
    `{"users":[{"name":"aiocat","id":0},{"name":"john","id":1}]}`
);

console.log(toSnug(source)); // users [(? name "aiocat"  id 0  ) (? name "john"  id 1  )]
```

<hr>

## Found a bug? Got an error?

Please create a new issue on gitlab repository.

## Contributing

If you want to contribute to this project:

-   Please do not something useless.
-   Use the .prettierrc format file for prettier.

## Authors

-   [Aiocat](https://gitlab.com/aiocat)

## License

This project is distributed with [MIT](/LICENSE) license
