class Snug {
    constructor(snug_str) {
        this.data = snug_str + " \n ";
        this.collectedToken = "";
        this.convertedJson = "{";

        this.collectingString = false;
        this.collectingLiteral = false;
        this.collectingComment = false;

        this.tokens = [];
        this.ignoreIndexs = [];
    }

    getCharOf(index) {
        if (index < this.data.length - 1) {
            return this.data[index];
        }

        return "\0";
    }

    collectLiteral() {
        // Remove newline from token
        this.collectedToken = this.collectedToken.replace(/(\r\n|\n|\r)/gm, "");
        if (this.collectedToken.length == 0) return;

        // Check token type
        if (!isNaN(parseInt(this.collectedToken.replace(/_/g, "")))) {
            this.tokens.push({
                key: "NUMBER",
                value: parseInt(this.collectedToken.replace(/_/g, "")),
            });
        } else if (!isNaN(parseFloat(this.collectedToken))) {
            this.tokens.push({
                key: "FLOAT",
                value: parseFloat(this.collectedToken),
            });
        } else if (this.collectedToken == "yes") {
            this.tokens.push({
                key: "BOOL_YES",
                value: "",
            });
        } else if (this.collectedToken == "no") {
            this.tokens.push({
                key: "BOOL_NO",
                value: "",
            });
        } else if (this.collectedToken == "(?") {
            this.tokens.push({
                key: "OPEN_ANONYMOUS_OBJECT",
                value: "",
            });
        } else if (this.collectedToken[0] == "(") {
            this.tokens.push({
                key: "OPEN_OBJECT",
                value: this.collectedToken.slice(1),
            });
        } else if (this.collectedToken == "nil") {
            this.tokens.push({
                key: "NIL",
                value: "",
            });
        } else {
            this.tokens.push({
                key: "UNKNOWN",
                value: this.collectedToken,
            });
        }

        this.collectedToken = "";
        this.collectingLiteral = false;
    }

    lexerizeData() {
        // Iterate over data
        this.data.split("").forEach((v, i) => {
            if (!this.collectingString && !this.collectingComment) {
                switch (this.getCharOf(i)) {
                    case "[":
                        this.collectLiteral();
                        this.tokens.push({
                            key: "BRACKET_OPEN",
                            value: "",
                        });
                        break;
                    case "]":
                        this.collectLiteral();
                        this.tokens.push({
                            key: "BRACKET_CLOSE",
                            value: "",
                        });
                        break;
                    case ")":
                        this.collectLiteral();
                        this.tokens.push({
                            key: "BRACE_CLOSE",
                            value: "",
                        });
                        break;
                    case '"':
                        // Enable string mode
                        this.collectingString = true;
                        break;
                    case "#":
                        // Enable comment mode
                        this.collectingComment = true;
                        break;
                    case " ":
                    case "\t":
                        this.collectLiteral();
                        break;
                    case "\r":
                        break;
                    case "\n":
                        this.collectLiteral();
                    default:
                        // Collect token
                        this.collectingLiteral = true;
                        this.collectedToken += this.getCharOf(i);
                }
            } else if (!this.collectingComment) {
                switch (this.getCharOf(i)) {
                    case '"':
                        if (this.getCharOf(i - 1) == "\\") {
                            this.collectedToken += '\\"';
                        } else {
                            this.tokens.push({
                                key: "STRING",
                                value: this.collectedToken,
                            });

                            this.collectingString = false;
                            this.collectedToken = "";
                        }
                        break;
                    default:
                        this.collectedToken += this.getCharOf(i);
                }
            } else {
                if (this.getCharOf(i) == "#") {
                    this.collectingComment = false;
                }
            }
        });
    }

    handleString(token) {
        return `"${token.value}"`;
    }

    valueConverter(token) {
        if (token.key == "STRING") return this.handleString(token);
        else if (token.key == "NUMBER" || token.key == "FLOAT")
            return token.value.toString();
        else if (token.key == "BOOL_YES") return "true";
        else if (token.key == "BOOL_NO") return "false";
        else if (token.key == "NIL") return "null";
    }

    // Parse anonymous object
    parseAnonObject(gotIndex) {
        this.convertedJson += `{`;

        for (let index = gotIndex; index < this.tokens.length; index++) {
            let token = this.tokens[index];

            if (this.ignoreIndexs.includes(index)) continue;
            else this.ignoreIndexs.push(index);

            if (token.key == "OPEN_OBJECT") this.parseObject(index, token);
            else if (token.key == "BRACE_CLOSE") {
                if (
                    this.tokens[index - 1].key != "OPEN_ANONYMOUS_OBJECT" &&
                    this.convertedJson[this.convertedJson.length - 1] != "}"
                )
                    this.convertedJson = this.convertedJson.slice(0, -1) + "},";
                else this.convertedJson += "},";

                break;
            } else if (token.key == "BRACKET_OPEN") this.parseArray(index);
            else if (
                token.key == "UNKNOWN" &&
                ![
                    "BRACKET_OPEN",
                    "OPEN_ANONYMOUS_OBJECT",
                    "OPEN_BRACE",
                ].includes(this.tokens[index + 1].key)
            )
                this.convertedJson += `"${token.value}":${this.valueConverter(
                    this.tokens[index + 1]
                )},`;
        }
    }

    // Parse anonymous array
    parseAnonArray(gotIndex) {
        this.convertedJson += "[";

        for (let index = gotIndex + 1; index < this.tokens.length; index++) {
            let token = this.tokens[index];

            if (this.ignoreIndexs.includes(index)) continue;
            else this.ignoreIndexs.push(index);

            if (token.key == "OPEN_ANONYMOUS_OBJECT")
                this.parseAnonObject(index);
            else if (token.key == "BRACKET_CLOSE") {
                if (this.tokens[index - 1].key != "BRACKET_OPEN")
                    this.convertedJson = this.convertedJson.slice(0, -1) + "],";
                else this.convertedJson += "],";

                break;
            } else if (token.key == "BRACKET_OPEN") this.parseAnonArray(index);
            else
                this.convertedJson += `${this.valueConverter(
                    this.tokens[index]
                )},`;
        }
    }

    // Parse array
    parseArray(gotIndex) {
        this.convertedJson += `"${this.tokens[gotIndex - 1].value}":[`;

        for (let index = gotIndex + 1; index < this.tokens.length; index++) {
            let token = this.tokens[index];

            if (this.ignoreIndexs.includes(index)) continue;
            else this.ignoreIndexs.push(index);

            if (token.key == "OPEN_ANONYMOUS_OBJECT")
                this.parseAnonObject(index);
            else if (token.key == "BRACKET_CLOSE") {
                if (
                    this.tokens[index - 1].key != "BRACKET_OPEN" &&
                    this.convertedJson[this.convertedJson.length - 1] !== "}"
                )
                    this.convertedJson = this.convertedJson.slice(0, -1) + "],";
                else this.convertedJson += "],";

                break;
            } else if (token.key == "BRACKET_OPEN") this.parseAnonArray(index);
            else
                this.convertedJson += `${this.valueConverter(
                    this.tokens[index]
                )},`;
        }
    }

    // Parse object
    parseObject(gotIndex, gotToken) {
        this.convertedJson += `"${gotToken.value}":{`;

        for (let index = gotIndex; index < this.tokens.length; index++) {
            let token = this.tokens[index];

            if (this.ignoreIndexs.includes(index)) continue;
            else this.ignoreIndexs.push(index);

            if (token.key == "OPEN_OBJECT") this.parseObject(index, token);
            else if (token.key == "BRACE_CLOSE") {
                if (
                    this.tokens[index - 1].key != "OPEN_ANONYMOUS_OBJECT" &&
                    this.convertedJson[this.convertedJson.length - 1] != "}"
                )
                    this.convertedJson = this.convertedJson.slice(0, -1) + "},";
                else this.convertedJson += "},";

                break;
            } else if (token.key == "BRACKET_OPEN") this.parseArray(index);
            else if (
                token.key == "UNKNOWN" &&
                ![
                    "BRACKET_OPEN",
                    "OPEN_ANONYMOUS_OBJECT",
                    "OPEN_BRACE",
                ].includes(this.tokens[index + 1].key)
            )
                this.convertedJson += `"${token.value}":${this.valueConverter(
                    this.tokens[index + 1]
                )},`;
        }
    }

    // Convert snug source to the raw JSON
    toJson() {
        for (let index = 0; index < this.tokens.length; index++) {
            let token = this.tokens[index];

            if (this.ignoreIndexs.includes(index)) continue;
            else this.ignoreIndexs.push(index);

            if (token.key == "OPEN_OBJECT") this.parseObject(index, token);
            else if (token.key == "BRACKET_OPEN") this.parseArray(index, token);
            else if (
                token.key == "UNKNOWN" &&
                ![
                    "BRACKET_OPEN",
                    "OPEN_ANONYMOUS_OBJECT",
                    "OPEN_BRACE",
                ].includes(this.tokens[index + 1].key)
            )
                this.convertedJson += `"${token.value}":${this.valueConverter(
                    this.tokens[index + 1]
                )},`;
        }

        if (this.convertedJson[this.convertedJson.length - 1] == "}")
            this.convertedJson += "}";
        else this.convertedJson = this.convertedJson.slice(0, -1) + "}";
    }

    parseJson() {
        this.lexerizeData();
        this.toJson();
        return this.convertedJson;
    }
}

function toSnugType(key, val) {
    if (typeof val == "string") return `"${val}" `;
    else if (val == null) return `nil `;
    else if (Array.isArray(val))
        return `[${val.map((i) => toSnugType(undefined, i)).join(" ")}]`;
    else if (typeof val == "object" && key == undefined) {
        let collected = "(? ";

        for (const inside in val) {
            if (
                typeof val[inside] === "object" &&
                !Array.isArray(val[inside]) &&
                val[inside] !== null
            )
                collected += `${toSnugType(inside, val[inside])}`;
            else collected += `${inside} ${toSnugType(inside, val[inside])} `;
        }

        return `${collected})`;
    } else if (typeof val == "object") {
        let collected = `(${key} `;

        for (const inside in val) {
            if (
                typeof val[inside] === "object" &&
                !Array.isArray(val[inside]) &&
                val[inside] !== null
            )
                collected += `${toSnugType(inside, val[inside])}`;
            else collected += `${inside} ${toSnugType(inside, val[inside])} `;
        }

        return `${collected})`;
    } else return `${val} `;
}

// Convert javascript object to the raw snug source
function toSnug(givenObj) {
    let collected = "";

    for (const obj in givenObj) {
        let keyVal = givenObj[obj];

        if (
            typeof keyVal === "object" &&
            !Array.isArray(keyVal) &&
            keyVal !== null
        )
            collected += `${toSnugType(obj, keyVal)}`;
        else collected += `${obj} ${toSnugType(obj, keyVal)}`;
    }

    return collected;
}
